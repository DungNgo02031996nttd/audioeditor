package com.example.asus.audioediter

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import com.example.asus.audioediter.ui.main.MainFragment

class HomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        initControl();
    }
    private fun initControl() {
        val mainFragment = MainFragment()
        pushFragment(mainFragment)
    }

    fun pushFragment(fragment: Fragment) {
        val manager: FragmentManager = supportFragmentManager
        manager.beginTransaction()
                .replace(R.id.container, fragment)
                .commit()
    }
}
