package com.pdfscan.pdfscannerfree.camscanner.ui

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.view.View

abstract class BaseFragment : Fragment() {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView()

    }

    abstract fun setupView()

    fun onBack() {
        val manager: FragmentManager = activity!!.supportFragmentManager
        manager.popBackStack()
    }

}