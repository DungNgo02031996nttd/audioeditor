package com.example.asus.audioediter.ui.main

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.asus.audioediter.R
import com.example.asus.audioediter.ui.edit.EditFragment
import com.pdfscan.pdfscannerfree.camscanner.ui.BaseFragment
import kotlinx.android.synthetic.main.fragment_main.*

class MainFragment : BaseFragment(),View.OnClickListener{

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v: View = LayoutInflater.from(activity!!).inflate(R.layout.fragment_main, container, false);
        return v;
    }
    override fun setupView() {
        mImgEditor.setOnClickListener(this)
        mImgMerge.setOnClickListener(this)
        mImgRecord.setOnClickListener(this)
        mImgFolder.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.mImgEditor -> {
//                var fragment = EditFragment()
//                val manager = activity!!.supportFragmentManager
//                manager.beginTransaction()
//                        .add(R.id.container, fragment)
//                        .addToBackStack(null)
//                        .commit()

            }
            R.id.mImgMerge -> {

            }
            R.id.mImgRecord -> {

            }
            R.id.mImgFolder -> {

            }
        }
    }

}