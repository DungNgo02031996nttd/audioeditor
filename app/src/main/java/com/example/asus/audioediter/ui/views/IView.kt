package com.pdfscan.pdfscannerfree.camscanner.ui.views

interface IView<P> {
    fun setPresenter(presenter: P)
}